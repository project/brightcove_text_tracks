<?php

namespace Drupal\brightcove_text_tracks;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

// @note: You only need Reference, if you want to change service arguments.
use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies the Brightcove Ingesion service.
 */
class BrightcoveTextTracksServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides brightcove.ingestion class to include transcriptions.
    if ($container->hasDefinition('brightcove.ingestion')) {
      $definition = $container->getDefinition('brightcove.ingestion');
      $definition->setClass('Drupal\brightcove_text_tracks\Services\Ingestion')
        ->addArgument(new Reference('entity_type.manager'))
        ->addArgument(new Reference('image.factory'))
        ->addArgument(new Reference('keyvalue.expirable'))
        ->addArgument(new Reference('brightcove.logger'))
        ->addArgument(new Reference('datetime.time'));
    }
  }

}
