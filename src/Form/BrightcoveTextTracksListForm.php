<?php

namespace Drupal\brightcove_text_tracks\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\brightcove\BrightcoveUtil;
use Brightcove\Item\Video\TextTrack;
use Brightcove\Item\Video\TextTrackSource;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Markup;

/**
 * Form for Text tracks List form.
 *
 * @package Drupal\brightcove_text_tracks\Form
 */
class BrightcoveTextTracksListForm extends FormBase {

  /**
   * Get access to messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor to get services.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('messenger'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brightcove_text_tracks_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $brightcove_video = NULL) {
    $text_tracks = $this->buildTextTrackListing($brightcove_video, $form_state);
    $form['text_tracks_list'] = $text_tracks;
    $form['action_bar'] = [
      '#type' => 'container',
      '#weight' => 500,
      '#attributes' => [
        'id' => 'action-bar',
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#weight' => 500,
      '#access' => !empty($text_tracks['text_tracks']['#options']),
    ];
    if (empty($form['text_tracks_list']['text_tracks']['#options'])) {
      $form['text_tracks_list']['text_tracks']['#type'] = 'table';
      $form['text_tracks_list']['text_tracks']['#rows'][] = [$this->t('No text tracks available')];
    }

    return $form;
  }

  /**
   * List the text tracks available for the video.
   */
  private function buildTextTrackListing($brightcove_video, &$form_state) {
    $build_info = $form_state->getBuildInfo();
    $cms = BrightcoveUtil::getCmsApi($brightcove_video->getApiClient());
    $video = $cms->getVideo($brightcove_video->getBrightcoveId());
    $text_tracks = $video->getTextTracks();
    $header = ['SRC', 'Language', 'Kind', 'Status', 'Edit'];
    $rows = [];
    foreach ($text_tracks as $text_track) {
      $text_track_id = $text_track->getID();
      $build_info['text_tracks'][$text_track_id] = $text_track;
      $rows[$text_track_id] = [
        Markup::create(
          '<a target="_blank" href="' . urldecode($text_track->getSrc()) . '">' .
          $text_track->getLabel() . '</a>'
        ),
        $text_track->getSrclang(),
        $text_track->getKind(),
        $text_track->getStatus(),
        Link::fromTextAndUrl($this->t('Edit'),
        Url::fromRoute('brightcove_text_tracks_edit',
        [
          'brightcove_video' => $brightcove_video->id(),
          'text_track' => $text_track_id,
        ])),
      ];
    }
    $form_state->setBuildInfo($build_info);
    $build = [
      'text_tracks' => [
        '#prefix' => '<h1>Brightcove Text tracks</h1>',
        '#type' => 'tableselect',
        '#attributes' => [
          'data-striping' => 0,
        ],
        '#header' => $header,
        '#options'   => $rows,
      ],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $entity = $build_info['args'][0];
    $text_tracks_to_delete = $form_state->getValue('text_tracks');
    $text_tracks_to_delete = array_filter($text_tracks_to_delete);
    $original_text_tracks = array_keys($build_info['text_tracks']);
    $updated_text_tracks = array_diff($original_text_tracks, $text_tracks_to_delete);

    foreach ($updated_text_tracks as $text_track_id) {
      $text_track = $build_info['text_tracks'][$text_track_id];
      $video_text_track = (new TextTrack())
        ->setId($text_track->getId())
        ->setSrclang($text_track->getSrclang())
        ->setLabel($text_track->getLabel())
        ->setKind($text_track->getKind())
        ->setMimeType($text_track->getMimeType())
        ->setAssetId($text_track->getAssetId());

      // If asset ID is set the src will be ignored, so in this case
      // we don't set the src.
      if (!empty($text_track->getAssetId())) {
        $video_text_track->setAssetId($text_track->getAssetId());
      }
      // Otherwise, set the src.
      // Get text track sources.
      $video_text_track_sources = [];
      foreach ($text_track->getSources() as $source) {
        $text_track_source = new TextTrackSource();
        $text_track_source->setSrc($source->getSrc());
        $video_text_track_sources[] = $text_track_source;
      }
      $video_text_track->setSources($video_text_track_sources);

      $video_text_tracks[] = $video_text_track;
    }
    $cms = BrightcoveUtil::getCmsApi($entity->getApiClient());
    $video = $cms->getVideo($entity->getBrightcoveId());
    $video->setTextTracks($video_text_tracks);
    if ($cms->updateVideo($video)) {
      $this->messenger->addMessage($this->t('The selected text tracks have been deleted'));
    }
  }

}
