<?php

namespace Drupal\brightcove_text_tracks\Form;

use Drupal\file\Entity\File;
use Drupal\Core\Form\FormBase;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\Entity\BrightcoveTextTrack;
use Drupal\Core\Form\FormStateInterface;
use Brightcove\Item\Video\TextTrack;
use Brightcove\Item\Video\TextTrackSource;
use Brightcove\API\Request\IngestTranscriptions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\brightcove\Services\IngestionInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Form for Text tracks add edit form.
 *
 * @package Drupal\brightcove_text_tracks\Form
 */
class AddTextTrackForm extends FormBase {

  /**
   * Get access to ingestion service.
   *
   * @var Drupal\brightcove\Services\IngestionInterface
   */
  protected $ingestService;

  /**
   * Get access to messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor to get services.
   */
  public function __construct(IngestionInterface $ingestService, MessengerInterface $messenger) {
    $this->ingestService = $ingestService;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('brightcove.ingestion'),
          $container->get('messenger'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brightcove_text_track_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $brightcove_video = NULL, $text_track = NULL) {
    $cms = BrightcoveUtil::getCmsApi($brightcove_video->getApiClient());
    $video = $cms->getVideo($brightcove_video->getBrightcoveId());
    // $video = $cms->getVideo(6317807059112);
    $text_tracks = $video->getTextTracks();
    foreach ($text_tracks as $track) {
      $text_track_id = $track->getID();
      $arr_text_tracks['text_tracks'][$text_track_id] = $track;
      $build_info[$text_track_id] = [
        'src' => $track->getSrc(),
        'srclang' => $track->getSrclang(),
        'label' => $track->getLabel(),
        'kind' => $track->getKind(),
        'status' => $track->getStatus(),
        'default' => $track->isDefault(),
      ];
    }
    $form_state->addBuildInfo('text_tracks', $arr_text_tracks);

    $form['webvtt_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Webvtt file:'),
      '#default_value' => '',
      '#upload_location' => 'public://text_tracks/',
      '#upload_validators' => [
        'file_validate_extensions' => ['vtt scc srt dfxp'],
      ],
      '#required' => isset($text_track),
      '#description' => $this->t('Allowed extensions: vtt scc srt dfxp'),
    ];
    if (!$text_track) {
      $form['autocaption'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Autocaption:'),
        '#default_value' => '',
      ];
    }
    $form['language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language:'),
      '#default_value' => isset($build_info[$text_track]['srclang']) ?
      $build_info[$text_track]['srclang'] : "",
      '#description' => $this->t('ISO-639-1 language code with optional ISO-3166 country name (en, en-US, de, de-DE).'),
      '#required' => isset($text_track),
    ];
    if (!isset($text_track)) {
      $form['language']['#description'] = $form['language']['#description']
      . ' ' . $this->t('For Autocaption, language will be autodetected if left blank.');
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label:'),
      '#default_value' => isset($build_info[$text_track]['label']) ?
      $build_info[$text_track]['label'] : "",
      '#description' => $this->t('Title to be displayed in the player menu'),
      '#required' => TRUE,
    ];
    $form['kind'] = [
      '#type' => 'select',
      '#title' => $this->t('Kind'),
      '#required' => TRUE,
      '#options' => [
        BrightcoveTextTrack::KIND_CAPTIONS => BrightcoveTextTrack::KIND_CAPTIONS,
        BrightcoveTextTrack::KIND_SUBTITLES => BrightcoveTextTrack::KIND_SUBTITLES,
        BrightcoveTextTrack::KIND_DESCRIPTION => BrightcoveTextTrack::KIND_DESCRIPTION,
        BrightcoveTextTrack::KIND_CHAPTERS => BrightcoveTextTrack::KIND_CHAPTERS,
        BrightcoveTextTrack::KIND_METADATA => BrightcoveTextTrack::KIND_METADATA,
      ],
      '#description' => $this->t('How the vtt file is meant to be used.'),
      '#default_value' => isset($build_info[$text_track]['kind']) ?
      $build_info[$text_track]['kind'] : "",
    ];
    $form['default_text_track'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default'),
      '#default_value' => isset($build_info[$text_track]['default']) ?
      $build_info[$text_track]['default'] : "",
    ];
    $form['published'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Published'),
      '#default_value' => $build_info[$text_track]['status'] == "published",
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $text_track_id = isset($build_info['args'][1]) ? $build_info['args'][1] : NULL;
    if (!isset($text_track_id)) {
      $webvtt_file = $form_state->getValue('webvtt_file');
      $autocaption = $form_state->getValue('autocaption');
      $language = $form_state->getValue('language');
      if (empty($webvtt_file[0]) && empty($autocaption)) {
        $form_state->setError($form, $this->t("Either Webvtt file or Autocaption is required."));
      }
      elseif (!empty($webvtt_file[0]) && empty(trim($language))) {
        $form_state->setError($form['language'], $this->t("Language is required."));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ingest_request = NULL;
    $build_info = $form_state->getBuildInfo();
    $text_track_id = isset($build_info['args'][1]) ? $build_info['args'][1] : '';
    $entity = $build_info['args'][0];
    $form_values = $form_state->getValues();

    if (!empty($form_values['webvtt_file'][0])) {
      $file = File::load($form_values['webvtt_file'][0]);
      $file_uri = file_create_url($file->getFileUri());
    }
    if (empty($text_track_id)) {
      if ($form_values['autocaption'] == 1) {
        $ingest_transcription = (new IngestTranscriptions())
          ->setLabel($form_values['label'])
          ->setKind((string) $form_values['kind'])
          ->setDefault((bool) $form_values['default_text_track']);
        if (!empty($form_values['language'])) {
          $ingest_transcription->setSrclang($form_values['language']);
        }
        else {
          $ingest_transcription->setAutoDetect(TRUE);
        }
        if ($form_values['published']) {
          $ingest_transcription->setStatus('published');
        }
        else {
          $ingest_transcription->setStatus('draft');
        }
        $ingest_transcriptions[] = $ingest_transcription;
        $di = BrightcoveUtil::getDiApi($entity->getApiClient());

        $ingest_request = $ingest_request ?? $this->ingestService->createIngestRequest();
        $ingest_request->setTranscriptions($ingest_transcriptions);
        $response = $di->createIngest($entity->getBrightcoveId(), $ingest_request);
        if ($response->getId()) {
          $this->messenger->addMessage($this->t('The text tracks are updated for the Video.'));
          $form_state->setRedirect('brightcove_text_tracks_list', ['entity' => $entity->id()]);
        }
        else {
          $this->messenger->addError($this->t('Failed to create text track for the Video'));
          $form_state->setRedirect('brightcove_text_tracks_list', ['entity' => $entity->id()]);
        }
      }
    }
    $cms = BrightcoveUtil::getCmsApi($entity->getApiClient());
    $video = $cms->getVideo($entity->getBrightcoveId());
    $text_tracks = $video->getTextTracks();
    foreach ($text_tracks as $text_track) {
      if ($text_track_id != $text_track->getId()) {
        $video_text_track = (new TextTrack())
          ->setId($text_track->getId())
          ->setSrclang($text_track->getSrclang())
          ->setLabel($text_track->getLabel())
          ->setKind($text_track->getKind());
        // If asset ID is set the src will be ignored, so in this case
        // we don't set the src.
        if (!empty($text_track->getAssetId())) {
          $video_text_track->setAssetId($text_track->getAssetId());
        }
        // Otherwise, set the src.
        // Get text track sources.
        $video_text_track_sources = [];
        foreach ($text_track->getSources() as $source) {
          $text_track_source = new TextTrackSource();
          $text_track_source->setSrc($source->getSrc());
          $video_text_track_sources[] = $text_track_source;
        }
        $video_text_track->setSources($video_text_track_sources);
        $video_text_tracks[] = $video_text_track;
      }
      else {
        $published = $form_values['published'] ? "published" : "draft";
        $video_text_tracks[] = (new TextTrack())
          ->setId($text_track->getId())
          ->setSrclang($form_values['language'])
          ->setLabel($form_values['label'])
          ->setKind($form_values['kind'])
          ->setSrc($file_uri)
          ->setDefault((bool) $form_values['default_text_track'])
          ->setStatus($published);
      }
    }
    $video->setTextTracks($video_text_tracks);
    $saved_video = $cms->updateVideo($video);
    if (isset($saved_video)) {
      $this->messenger->addMessage($this->t('The text tracks are updated for the Video.'));
      $form_state->setRedirect('brightcove_text_tracks_list', ['entity' => $entity->id()]);
    }
  }

}
