<?php

declare(strict_types = 1);

namespace Drupal\brightcove_text_tracks\Services;

use Drupal\brightcove\Services\IngestionInterface;
use Drupal\brightcove\Services\LoggerInterface;
use Drupal\brightcove\Services\SettingsInterface;
use Brightcove\API\Request\IngestImage;
use Brightcove\API\Request\IngestRequest;
use Brightcove\API\Request\IngestRequestMaster;
use Brightcove\API\Request\IngestTextTrack;
use Brightcove\API\Request\IngestTranscriptions;
use Brightcove\API\Response\IngestResponse;
use Drupal\brightcove\BrightcoveUtil;
use Drupal\brightcove\BrightcoveVideoInterface;
use Drupal\brightcove\Entity\BrightcoveVideo;
use Drupal\brightcove\Module;
use Drupal\brightcove\Services\Exception\IngestionException;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\file\FileInterface;

/**
 * Ingestion helper service.
 */
final class Ingestion implements IngestionInterface {

  /**
   * Callback token storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  private $callbackTokenStorage;

  /**
   * File storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $fileStorage;

  /**
   * Image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  private $imageFactory;

  /**
   * Module logger.
   *
   * @var \Drupal\brightcove\Services\LoggerInterface
   */
  private $logger;

  /**
   * Module settings.
   *
   * @var \Drupal\brightcove\Services\SettingsInterface
   */
  private $settings;

  /**
   * Text Track storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $textTrackStorage;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Initializes an ingestion helper.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   Image factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_expirable_factory
   *   Key-value expirable factory.
   * @param \Drupal\brightcove\Services\LoggerInterface $logger
   *   Logger.
   * @param \Drupal\brightcove\Services\SettingsInterface $settings
   *   Module settings.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ImageFactory $image_factory, KeyValueExpirableFactoryInterface $key_value_expirable_factory, LoggerInterface $logger, SettingsInterface $settings, TimeInterface $time) {
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->textTrackStorage = $entity_type_manager->getStorage('brightcove_text_track');
    $this->imageFactory = $image_factory;
    $this->callbackTokenStorage = $key_value_expirable_factory->get(Module::NAME . '_callback');
    $this->logger = $logger;
    $this->settings = $settings;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function createIngestRequest(): IngestRequest {
    return new IngestRequest();
  }

  /**
   * Create an ingestion request for an image.
   *
   * @param \Drupal\file\FileInterface $file
   *   File entity.
   *
   * @return \Brightcove\API\Request\IngestImage
   *   Image ingestion object.
   */
  private function createIngestImage(FileInterface $file): ?IngestImage {
    // Set up image ingestion.
    // Load the image object, so we can access height and width.
    $image = $this->imageFactory->get($file->getFileUri());
    if ($image !== NULL) {
      $ingest_image = new IngestImage();
      $ingest_image->setUrl(file_create_url($file->getFileUri()));
      $ingest_image->setWidth($image->getWidth());
      $ingest_image->setHeight($image->getHeight());
      return $ingest_image;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getIngestionToken(BrightcoveVideoInterface $video): ?string {
    try {
      // Generate unique token.
      do {
        $token = Crypt::hmacBase64($video->getBrightcoveId(), Crypt::randomBytesBase64() . Settings::getHashSalt());
      } while ($this->callbackTokenStorage->has($token));

      // Insert unique token into database.
      $this->callbackTokenStorage->setWithExpire($token, $video->id(), $this->settings->getNotificationCallbackExpirationTime());
    }
    catch (\Exception $e) {
      $this->logger->logException($e, 'Failed to generate ingestion token.');

      // Reset token to NULL.
      $token = NULL;
    }

    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageIngestion(?array $image): ?IngestImage {
    $file_id = $image['target_id'] ?? NULL;
    if ($file_id !== NULL) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->fileStorage->load($file_id);
      if (!empty($file)) {
        return $this->createIngestImage($file);
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function sendIngestVideo(BrightcoveVideoInterface $video): ?IngestResponse {
    /** @var \Brightcove\API\Request\IngestRequest|null $ingest_request */
    $ingest_request = NULL;

    if ($video->isFieldChanged('text_tracks')) {
      $ingest_text_tracks = [];
      $text_tracks = $video->getTextTracks();
      $text_track_ids = [];
      foreach ($text_tracks as $text_track) {
        if (!empty($text_track['target_id'])) {
          $text_track_ids[] = $text_track['target_id'];
        }
      }
      /** @var \Drupal\brightcove\BrightcoveTextTrackInterface $text_tracks */
      $text_tracks = $this->textTrackStorage->loadMultiple($text_track_ids);

      foreach ($text_tracks as $text_track) {
        // Setup ingestion request if there was a text track uploaded.
        $web_vtt_file = $text_track->getWebVttFile();
        if (!empty($web_vtt_file[0]['target_id'])) {
          /** @var \Drupal\file\Entity\File $file */
          $file = $this->fileStorage->load($web_vtt_file[0]['target_id']);

          if ($file !== NULL) {
            $ingest_text_tracks[] = (new IngestTextTrack())
              ->setSrclang($text_track->getSourceLanguage())
              ->setUrl(file_create_url($file->getFileUri()))
              ->setKind($text_track->getKind())
              ->setLabel($text_track->getLabel())
              ->setDefault($text_track->isDefault());
          }
        }
        else {
          $ingest_request = NULL;
          $ingest_transcriptions[] = (new IngestTranscriptions())
            ->setSrclang($text_track->getSourceLanguage())
            ->setKind($text_track->getKind())
            ->setLabel($text_track->getLabel())
            ->setDefault($text_track->isDefault());
        }
      }

      // Set ingestion for text tracks.
      if ($ingest_text_tracks !== []) {
        $ingest_request = $ingest_request ?? $this->createIngestRequest();
        $ingest_request->setTextTracks($ingest_text_tracks);
      }
      if ($ingest_transcriptions !== []) {
        $ingest_request = $ingest_request ?? $this->createIngestRequest();
        $ingest_request->setTranscriptions($ingest_transcriptions);
      }
    }

    // Set ingestion for thumbnail.
    if ($video->isFieldChanged(BrightcoveVideoInterface::IMAGE_TYPE_THUMBNAIL)) {
      $thumbnail = $this->getImageIngestion($video->getThumbnail());
      if ($thumbnail !== NULL) {
        $ingest_request = $ingest_request ?? $this->createIngestRequest();
        $ingest_request->setThumbnail($thumbnail);
      }
    }

    // Set ingestion for poster.
    if ($video->isFieldChanged(BrightcoveVideoInterface::IMAGE_TYPE_POSTER)) {
      $poster = $this->getImageIngestion($video->getPoster());
      if ($poster !== NULL) {
        $ingest_request = $ingest_request ?? $this->createIngestRequest();
        $ingest_request->setPoster($poster);
      }
    }

    // Set ingestion for video.
    $video_file = $video->getVideoFile();
    if (($video->isFieldChanged('profile') || $video->isFieldChanged('video_file')) && !empty($video_file['target_id'])) {
      /** @var \Drupal\file\Entity\File $file */
      $file = $this->fileStorage->load($video_file['target_id']);

      $ingest_request = $ingest_request ?? $this->createIngestRequest();
      $ingest_master = new IngestRequestMaster();
      $ingest_master->setUrl(file_create_url($file->getFileUri()));
      $ingest_request->setMaster($ingest_master);
    }

    // Set ingestion for video url.
    $video_url = $video->getVideoUrl();
    if (($video->isFieldChanged('profile') || $video->isFieldChanged('video_url')) && !empty($video_url)) {
      $ingest_request = $ingest_request ?? $this->createIngestRequest();
      $ingest_master = new IngestRequestMaster();
      $ingest_master->setUrl($video_url);
      $ingest_request->setMaster($ingest_master);
    }

    // Send the ingest request if there was an ingestible asset.
    if ($ingest_request !== NULL) {
      $profiles = BrightcoveVideo::getProfileAllowedValues($video->getApiClient());
      $ingest_request->setProfile($profiles[$video->getProfile()]);

      // Get token.
      $token = $this->getIngestionToken($video);

      // Make sure that the token is generated successfully. If there was an
      // error generating the token then be nice to Brightcove and don't set
      // the callback url, so it won't hit a wall by trying to notify us on a
      // non-valid URL.
      if ($token !== NULL) {
        $callback_url = Url::fromRoute('brightcove_ingestion_callback', ['token' => $token], ['absolute' => TRUE])->toString();
        $ingest_request->setCallbacks([
          $callback_url,
        ]);
      }

      // Send request.
      try {
        $di = BrightcoveUtil::getDiApi($video->getApiClient());
        return $di->createIngest($video->getBrightcoveId(), $ingest_request);
      }
      catch (\Exception $e) {
        // Remove images and text tracks in case of a failure.
        // fixme: Is there any better way to handle this?
        // It was not possible to handle it during the form submission as the
        // form cannot be rebuilt properly in the save step in case of an error.
        if ($video->isFieldChanged(BrightcoveVideoInterface::IMAGE_TYPE_THUMBNAIL)) {
          $video->setThumbnail(NULL);
        }
        if ($video->isFieldChanged(BrightcoveVideoInterface::IMAGE_TYPE_POSTER)) {
          $video->setPoster(NULL);
        }
        if (!empty($text_tracks)) {
          $text_tracks_to_keep = [];
          /** @var \Drupal\brightcove\BrightcoveTextTrackInterface $text_track */
          foreach ($text_tracks as $text_track) {
            if (!empty($text_track->getTextTrackId())) {
              $text_tracks_to_keep[] = [
                'target_id' => $text_track->id(),
              ];
            }
          }
          $video->setTextTracks($text_tracks_to_keep);
        }

        if ($video->hasChangedField()) {
          $video->save();
        }

        throw new IngestionException('Failed to create ingestion request for Video assets.', $e->getCode(), $e);
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isFieldMarkedForIngestion(BrightcoveVideoInterface $video, string $field): bool {
    $fields = $video->getMarkedForIngestion();

    // Check if the last ingestion was later than the allowed time limit and
    // return FALSE if already expired, IOW the field is no longer marked for
    // ingestion.
    if (($fields[static::LAST_INGESTION_TIME_STORAGE] ?? 0) + $this->settings->getMarkedFieldExpiry() < $this->time->getCurrentTime()) {
      return FALSE;
    }

    return !empty($fields[$field]);
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldMarkedForIngestion(BrightcoveVideoInterface $video, string $field, bool $mark_for_ingestion): BrightcoveVideoInterface {
    $fields = $video->getMarkedForIngestion();
    $fields[$field] = $mark_for_ingestion;

    $fields[static::LAST_INGESTION_TIME_STORAGE] = $this->time->getCurrentTime();

    return $video->set('marked_for_ingestion', $fields);
  }

}
