# Brightcove Text tracks
This module provides add on features to fully replicate the features available in the Brightcove Studio in the management of the text tracks for the Brightcove Video.

Using Brightcove Text Tracks module, the Admins will be able to better manage the text tracks such as using Auto captioning for the videos, upload captions in multiple formats, ability to publish the captions based on need etc.

## Features
An additional interface to show the listing of text tracks(Captions) with the option to edit a specific text track.

Ability to Create a new Captions using Auto captioning feature.

Ability to set a Text track(Caption) as draft or published. This is useful for captions using the Auto captioning feature. Admins can opt for setting the initial status as draft, so once the Autocaptioning is done, they can review, make changes to captions and upload the caption.

Allows upload of Captions in other supported formats such as .srt, .scc, .dfxp etc. other than .vtt
